# python script for sensor reading

import RPi.GPIO as GPIO
import dht11
import time
import datetime

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# from https://github.com/szazo/DHT11_Python/blob/master/dht11_example.py
instance = dht11.DHT11(pin=18)
instance2 = dht11.DHT11(pin=23)
instance3 = dht11.DHT11(pin=24)
tempe = 0

while True:
    # result = instance.read()
    # if result.is_valid():
    #     print("Last valid input: " + str(datetime.datetime.now()))
    #     print("Temperature: %d C" % result.temperature)
    #     print("Humidity: %d %%" % result.humidity)
    result = instance.read()
    result2 = instance2.read()
    result3 = instance3.read()
    # timet = datetime.datetime.now().time()

    '''
    Temperature
    '''
    if (result.is_valid() and result2.is_valid() and result3.is_valid()):
        tempe = (result.temperature + result2.temperature + result3.temperature)/3
        print "Ave 3"
        print tempe
    elif(result.is_valid() and result2.is_valid() and result3.is_valid() == False):
        tempe = (result.temperature + result2.temperature)/2
        print "Ave 1 and 2"
        print tempe
    elif(result.is_valid() and result3.is_valid() and result2.is_valid() == False):
        tempe = (result.temperature + result3.temperature)/2
        print "Ave 1 and 3"
        print tempe
    elif(result2.is_valid() and result3.is_valid() and result.is_valid() == False):
        tempe = (result2.temperature + result3.temperature)/2
        print "Ave 2 and 3"
        print tempe
    elif(result.is_valid() and result2.is_valid() == False and result3.is_valid() == False):
        tempe = result.temperature
        print "Temp1"
        print tempe
    elif(result2.is_valid() and result3.is_valid() == False and result.is_valid() == False):
        tempe = result2.temperature
        print "Temp2"
        print tempe
    elif(result3.is_valid() and result.is_valid() == False and result2.is_valid() == False):
        tempe = result3.temperature
        print "Temp3"
        print tempe
    else:
        print "No Data Read"

    time.sleep(1)

# from https://github.com/adafruit/Adafruit_Python_DHT/blob/master/examples/simpletest.py
# used for adafruit dht11

# if humidity is not None and temperature is not None:
#     print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
# else:
#     print('Failed to get reading. Try again!')