import RPi.GPIO as GPIO
import time
import dcmotor_test

# GPIO.setmode(GPIO)
GPIO.setwarnings(False)
# GPIO.setup(8,GPIO.OUT)
GPIO.setup(5,GPIO.OUT)
GPIO.setup(7,GPIO.OUT)

def sleeper(count):
	time.sleep(count)

def relayOn(self):
	print "relay On"
	GPIO.output(self,GPIO.HIGH)

def relayOff(self):
	print 'relay off'
	GPIO.output(self,GPIO.LOW)

try:
	# relayOn(7)
	# sleeper(10)
	# relayOff(7)

	pin = 7
	cnt = dcmotor_test.DCMotor(pin)
	print cnt.turnOn(pin)
	sleeper(10)
	print cnt.turnOff(pin)

except KeyboardInterrupt:
	GPIO.cleanup()

finally:
	GPIO.cleanup()
