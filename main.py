import matplotlib
matplotlib.use('module://kivy.garden.matplotlib.backend_kivy')
import kivy
import sqlalchemy
import datetime
import os.path
import sqlite3
import time
import RPi.GPIO as GPIO
import dht11

from database import *

from kivy.config import Config
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '630')
Config.set('graphics', 'height', '300')
from kivy.core.window import Window
Window.clearcolor = (1,1,1,1)

from functools import wraps
from hx711 import HX711
from heater import Heater
from dcmotor import DCMotor
from kivy.app import App
from kivy.base import runTouchApp
from kivy.clock import Clock
from kivy.properties import  ObjectProperty, BooleanProperty, StringProperty, ListProperty, AliasProperty, NumericProperty
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from kivy.uix.spinner import Spinner
from kivy.uix.textinput import TextInput
from kivy.uix.actionbar import ActionItem
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image

import matplotlib.pyplot as plt

from random import randint, uniform

from sqlalchemy import exists

fig, (ax,ax2) = plt.subplots(2,1, sharex=True)
xarr = []
yarr = []
zarr = []
canvas = fig.canvas

timer = 0
checkid = 0

counter = 0
wetw = 0
dryw = 0

setMoist = 0

lastw = 0

checkInitial = True

tempe = 30

iniDate = None
initialw = 0

finalMoist = 100
checkMotor = True
divider = 0

weight = 0
lastrw = 0
counter2 = 0

# controlH = Heater(4)
# controlD = DCMotor(17)

hx = HX711(5, 6)
hy = HX711(20,21)

hx.set_reading_format("LSB", "MSB")
hy.set_reading_format("LSB", "MSB")

hx.set_reference_unit(-395)
hy.set_reference_unit(-335)

hx.reset()
hy.reset()
hx.tare()
hy.tare()

instance = dht11.DHT11(pin=18)
instance2 = dht11.DHT11(pin=23)
instance3 = dht11.DHT11(pin=24)

class Display(BoxLayout):
    pass

class ScrOne(Screen):

    def warning_pop(self):
        app = App.get_running_app()
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='There is no preset selected'))
        closeButton = Button(text='Close',
            background_color=app.NEGATIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7],
            auto_dismiss=False)

        popup.open()
        closeButton.bind(on_press=popup.dismiss)

    def check_val1(self):
        '''for [Apply Button]'''
        app = App.get_running_app()

        if self.ids.search_preset.text != 'Press to search for saved presets...':
            print '*-- ApplyButton pressed --*'

            sel = self.ids.search_preset.text
            app.USER_SELECTION = sel

            self.manager.current = 'load_cacao'

            self.manager.get_screen('load_cacao').load_details()
        else:
            print 'no'
            ScrOne().warning_pop()

    def check_val2(self):
        '''for [Delete Button]'''
        if self.ids.search_preset.text != 'Press to search for saved presets...':
            print '*-- delete button pressed --*'

            app = App.get_running_app()

            app.USER_SELECTION = self.ids.search_preset.text

            layout = BoxLayout(orientation='vertical')
            layout2 = BoxLayout(orientation='horizontal')
            layout.add_widget(Label(text='Are you sure you want to delete?'))

            deleteButton = Button(text='Yes, delete it',
                background_color=app.POSITIVE_COLOR,
                background_normal=app.NEUTRAL
                )
            cancelButton = Button(text='Cancel',
                background_color=app.NEGATIVE_COLOR,
                background_normal=app.NEUTRAL
                )

            layout2.add_widget(deleteButton)
            layout2.add_widget(cancelButton)
            layout.add_widget(layout2)

            popup =Popup(title='Delete Preset',
                content=layout,
                size_hint=[0.5, 0.7],
                auto_dismiss=False)

            popup.open()

            deleteButton.bind(
                on_press=lambda x:ScrOne().delete_vars(),
                on_release=popup.dismiss
                )
            cancelButton.bind(on_press=popup.dismiss)

        else:
            print 'no'
            ScrOne().warning_pop()
            
    def delete_vars(self):
        app = App.get_running_app()
        sel = app.USER_SELECTION
        print 'selection {}'.format(sel)
        parent = session.query(UserPreferences).filter_by(user_batch_name=sel)

        for items in parent:
            session.delete(items)
            session.commit()

class ScrTwo(Screen):
    '''
    create new preset screen
    '''

    def reset_vals(self, inputs):
        inputs.batch_name.text = ''
        inputs.input_desired_moisture.value = 7.0
        inputs.input_cacao_type.text = 'Select Cacao Type'

    def change_screen(self, instance):
        instance.current = 'home'

    def saveSet(self):
        '''
        save new user preference statement
        '''

        app = App.get_running_app()
        set1 = UserPreferences(
            type_of_cacao=app.USER_BATCH_TYPE,
            desired_moisture=app.USER_BATCH_MOISTURE,
            user_batch_name=app.USER_BATCH_NAME
            )
        session.add(set1)
        session.commit()

    def warning_pop(self, text):
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='Please input a valid {}'.format(text)))
        closeButton = Button(text='Close')

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7])

        popup.open()
        closeButton.bind(on_press=popup.dismiss)

    def save_pop(self, instance, inputs):
        app = App.get_running_app()

        def __init__(self, **kwargs):
            super(save_pop, self).__init__(**kwargs)

        layout=BoxLayout(orientation='vertical')
        layout2=BoxLayout(orientation='horizontal')
        layout.add_widget(Label(text='Are you sure you want to save?'))

        savebutton = Button(text='Yes, save it!',
            background_color=app.POSITIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        closeButton = Button(
            text='No, let me edit',
            background_color=app.NEGATIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        layout2.add_widget(savebutton)
        layout2.add_widget(closeButton)
        layout.add_widget(layout2)

        popup = Popup(title='Save Preset',
            content=layout,
            size_hint=[0.5, 0.7],
            auto_dismiss=False)

        popup.open()
        savebutton.bind(
            on_release=popup.dismiss,
            on_press=lambda x:(
                ScrTwo().saveSet(),
                ScrTwo().change_screen(instance),
                ScrTwo().reset_vals(inputs)
            )
        )
        closeButton.bind(
            on_press=popup.dismiss,
        )

    def exist_pop(self):
        app = App.get_running_app()
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='This name already exists'))
        closeButton = Button(text='Close',
            background_color=app.NEGATIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7])

        popup.open()
        closeButton.bind(on_press=popup.dismiss)
    def get_vals(self):
        '''
        gets values from the input fields
        checks whether values are correct
        '''
        app = App.get_running_app()
        name = self.ids.batch_name.text
        desired_moisture = self.ids.input_desired_moisture_label.text
        cacao_type = self.ids.input_cacao_type.text

        if name and desired_moisture:
            if cacao_type != 'Select Cacao Type':
                check = session.query(exists().where(UserPreferences.user_batch_name.ilike(name))).scalar()
                if check == False:
                    app.USER_BATCH_NAME = name
                    app.USER_BATCH_MOISTURE = float(desired_moisture)
                    app.USER_BATCH_TYPE = cacao_type
                    
                    ScrTwo().save_pop(self.manager, self.ids)
                else:
                    ScrTwo().exist_pop()
            else:
                text = 'Cacao Type'
                ScrTwo().warning_pop(text)
        else:
            text = 'Batch Name'
            ScrTwo().warning_pop(text)

class ScrThree(Screen):
    '''
    cacao loading
    '''

    vals = ListProperty()
    batch_name = StringProperty()
    des_moist = StringProperty()
    c_type = StringProperty()
    live_weight = StringProperty()

    def change_text(self, value):
        self.batch_name = str(value)

    def load_details(self):
        app = App.get_running_app()
        set1 = session.query(UserPreferences).filter_by(user_batch_name = app.USER_SELECTION).first()

        self.batch_name = str(set1.user_batch_name)
        self.des_moist = str(set1.desired_moisture)
        self.c_type = str(set1.type_of_cacao)

        self.canvas.ask_update()

    def cleanAndExit():
        print "Cleaning..."
        GPIO.cleanup()
        print "Bye!"

    def read_weight(self, sec):
        global weight, hx, hy
        weight1 = max(0, float(hx.get_weight(5)))
        weight2 = max(0, float(hy.get_weight(5)))
        print weight1
        print weight2
        weight = weight1 + weight2
        weightstr = weight / 1000
        
        self.live_weight = str('%.2f'%weightstr) + ' kg'
        print self.live_weight

        hx.power_down()
        hx.power_up()

        hy.power_down()
        hy.power_up()

    def saveVals(self):
        global initialw
        app = App.get_running_app()
        initialw = float(weight)
        
    def update(self):
        '''
        calls the function that reads weight
        '''
        self.eventUpdate = Clock.schedule_interval(self.read_weight,1)

    def start(self):
        self.update()

    def onExit(self):
        # self.ids.start_button.disabled = True
        Clock.unschedule(self.eventUpdate)

class ScrFour(Screen):

    def initiateBatch(self):
        '''
        Instantiate a new Batch for the selected UserPreference
        '''

        global checkid, setMoist, iniDate

        app = App.get_running_app()

        print app.USER_SELECTION

        cacaot = ''
        dm = 0.0
        name = ''
        queryName = session.query(UserPreferences).filter_by(user_batch_name=app.USER_SELECTION)
        for data in queryName:
            cacaot = data.type_of_cacao
            dm = data.desired_moisture
            name = data.user_batch_name

        setMoist = dm

        iniDate = datetime.datetime.now()

        applyUserPref = BatchData(
            batch_type_of_cacao=cacaot,
            batch_desired_moisture_content=dm,
            batch_name=name,
            batch_datetime=iniDate)
        session.add(applyUserPref)
        session.commit()

        '''
        Saves current Batch ID for reference and assignment of foreign key
        '''
        checkid = applyUserPref.id
        iniDate = applyUserPref.batch_datetime   

    def checkDataSensor(self,sec):

        global checkInitial, tempe, checkMotor, hx, hy, timer, lastrw, counter2

        if checkMotor == False:
            self.turnMotor(1)
            checkMotor = True
        result = instance.read()
        result2 = instance2.read()
        result3 = instance3.read()
        timet = datetime.datetime.now().time()

        '''
        Temperature
        '''
        if (result.is_valid() and result2.is_valid() and result3.is_valid()):
            tempe = (result.temperature + result2.temperature + result3.temperature)/3
        elif(result.is_valid() and result2.is_valid() and result3.is_valid() == False):
            tempe = (result.temperature + result2.temperature)/2
        elif(result.is_valid() and result3.is_valid() and result2.is_valid() == False):
            tempe = (result.temperature + result3.temperature)/2
        elif(result2.is_valid() and result3.is_valid() and result.is_valid() == False):
            tempe = (result2.temperature + result3.temperature)/2
        elif(result.is_valid() and result2.is_valid() == False and result3.is_valid() == False):
            tempe = result.temperature
        elif(result2.is_valid() and result3.is_valid() == False and result.is_valid() == False):
            tempe = result2.temperature
        elif(result3.is_valid() and result.is_valid() == False and result2.is_valid() == False):
            tempe = result3.temperature
        else:
            pass

        if checkInitial == True:

            app = App.get_running_app()

            setsave = EnvVars(batch_info=checkid,temperature=tempe,dry_weight=weight,read_time=timet)
            session.add(setsave)
            session.commit()

            checkInitial = False
        else:
            '''
            Weight
            '''
            check = float(timer / 900.00)
            if check.is_integer() == True:
                self.turnMotor(2)
                weight1 = max(0, float(hx.get_weight(5)))
                weight2 = max(0, float(hy.get_weight(5)))
                val = (weight1 + weight2)
                if counter2 == 0:
                    lastrw = val
                    counter2 = 1
                elif counter2 == 1:
                    if val > lastrw:
                        val = lastrw - 1
                    else:
                        lastrw = val

                hx.power_down()
                hx.power_up()
                hy.power_down()
                hy.power_up()

                setsave = EnvVars(batch_info=checkid,temperature=tempe,dry_weight=val,read_time=timet)
                session.add(setsave)
                session.commit()

                time.sleep(5)
                timer = timer + 5
                checkMotor = False
            else:
            	pass

        self.temp.text = str(tempe)
        if (finalMoist < (100-divider) and finalMoist > setMoist):
            self.colorQ.source = "img/yellow2.png"
        elif finalMoist < setMoist:
            self.colorQ.source = "img/green2.png"

        '''Heater Control'''
        if timet.hour in range (6,19):
            if tempe >= 60:
                self.turnHeater(2)
            else:
                self.turnHeater(1)
        else:
            if tempe >= 30:
                self.turnHeater(2)
            else:
                self.turnHeater(1)


    def etime(self, sec):
        '''
        Setting Elapsed Time
        '''

        global timer

        timer += 1

        set_elapsedt = str(datetime.timedelta(seconds=timer))

        self.elapsedt.text = set_elapsedt

    def plot(self,sec):

        global ax, ax2, xarr, yarr, zarr, counter, wetw, dryw, lastw, finalMoist, z, count, setMoist, counter2, lastrw
        '''
        Checks for new data
        '''
        
        dataCheck = session.query(EnvVars).filter_by(batch_info=checkid,graphed=False).first()

        if not dataCheck:
            print 'No Data'
        else:
            if len(xarr) >20:
                xarr[:]=[]
                yarr[:]=[]
                zarr[:]=[]
            timet = datetime.datetime.now().time()
            timet = timet.strftime("%I:%M %p")

            self.asof.text = timet

            container = []
            container2 = []
            yd = 0
            zd = 0

            check = datetime.date.today()

            getd = session.query(EnvVars).filter_by(batch_info=checkid,graphed=False)

            for data in getd:
                yd = data.temperature
                zd = data.dry_weight

            '''
            Computes Moisture Content everchanging wetw
            ''' 
            if counter == 0:
                wetw = zd
                counter = counter + 1
            elif counter == 1:
                dryw = zd
                moistContent = ((wetw-dryw)/wetw) * 100
                finalMoist = finalMoist - moistContent
                wetw = dryw
                dryw = 0
                counter = counter + 1
            else:
                dryw = zd
                moistContent = ((wetw-dryw)/wetw) * 100
                finalMoist = finalMoist - moistContent
                wetw = dryw
                dryw = 0
                counter = 1


            '''
            Computes Moisture Content by wetw initial weight
            ''' 
            # if counter == 0:
            #     wetw = zd
            #     counter = counter + 1
            # else:
            #     dryw = zd
            #     moistContent = ((wetw-dryw)/wetw) * 100

            gett = session.query(EnvVars.read_time).filter_by(batch_info=checkid,graphed=False)

            for data in gett:
                container = data
            for item in container:
                container2 = [item.strftime('%H:%M')] # H:M
            
            xarr.extend(container2)
            yarr += [yd]
            zarr += [zd]

            ax.cla()
            ax2.cla()
            plt.xticks(rotation=70)
            ax.plot(xarr,yarr, '-o')
            ax2.plot(xarr,zarr, '-ro')

            ax.set_title('Temp')
            ax2.set_title('Weight')

            canvas.draw()

            self.temp.text = str(yd) + ' C'
            self.moist.text = str('%.2f'%finalMoist) + ' %'

            data = {'graphed': True}
            getd.update(data)
            session.commit()

            if (setMoist >= finalMoist):

                lastw = wetw

                Clock.unschedule(self.eventUpdate)
                Clock.unschedule(self.eventUpdateE)
                Clock.unschedule(self.eventSaveD)

                xarr[:] = []
                yarr[:] = []
                zarr[:] = []

                counter = 0
                wetw = 0
                dryw = 0

                setMoist = 0

                tempe = 30

                counter2 = 0
                lastrw = 0

                '''Test'''
                z = 10
                count = 0

                self.done_pop()
                '''Motor and Heater Off'''
                self.turnHeater(2)
                self.turnMotor(2)
            else:
                pass

    def update(self):
        self.eventUpdate = Clock.schedule_interval(self.plot,1)

    def savedata(self):
        self.eventSaveD = Clock.schedule_interval(self.checkDataSensor,1)

    def updateE(self):
        self.eventUpdateE = Clock.schedule_interval(self.etime,1)

    def done_pop(self):
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='Done Drying'))
        closeButton = Button(text='Close')

        layout.add_widget(closeButton)

        popup = Popup(title='Success!',
            content=layout,
            size_hint= [0.5, 0.7])

        popup.open()
        closeButton.bind(on_press=lambda x:self.setLast(),
                on_release=popup.dismiss)

    def setLast(self):
        global ax, ax2
        ax.cla()
        ax2.cla()
        self.graph.clear_widgets()
        self.clear_widgets()
        self.manager.current = 'last_screen'

    def turnHeater(self,command):
    	# global controlH
        controlH = Heater(4)
        if command == 1:
            controlH.turnOff(controlH,4) #(controlH,<pin number>)
        else:
            controlH.turnOn(controlH,4) #(controlH,<pin number>)

    def turnMotor(self,command):
    	# global controlD
        controlD = DCMotor(17)
        if command == 1:
            controlD.turnOff(controlD,17) #(controlD,<pin number>)
        else:
            controlD.turnOn(controlD,17) #(controlD,<pin number>)
    def start(self):
        global fig

        self.initiateBatch()
        '''Motor and Heater On'''
        self.turnHeater(1)
        self.turnMotor(1)

        dividend = 100 - setMoist
        divider = dividend / 2

        parent = BoxLayout(orientation = 'horizontal')
        self.graph = BoxLayout(orientation = 'vertical', padding = [0,10,0,0], size_hint = (2,1))
        stats = BoxLayout(orientation = 'vertical')
        stata = BoxLayout(orientation = 'vertical')

        self.graph.add_widget(canvas)

        stats.add_widget(Label(text = 'As of:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Current Temp:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Moisture Content:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Bean Quality:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Time Elapsed:', color=[0, 0, 0, 1]))

        self.asof = Label(text = '00:00', color=[0, 0, 0, 1])
        stata.add_widget(self.asof)
        self.temp = Label(text = 'C', color=[0, 0, 0, 1])
        stata.add_widget(self.temp)
        self.moist = Label(text = '100 %', color=[0, 0, 0, 1])
        stata.add_widget(self.moist)
        self.colorQ = Image(source='img/red2.png')
        stata.add_widget(self.colorQ)
        self.elapsedt = Label(text = '00:00', color=[0, 0, 0, 1])
        stata.add_widget(self.elapsedt)

        parent.add_widget(self.graph)
        parent.add_widget(stats)
        parent.add_widget(stata)
        self.add_widget(parent)
        fig.tight_layout()

        self.savedata()

        self.update()
        self.updateE()

class LastScr(Screen):

    def load_details(self):
        app = App.get_running_app()
        set_elapsedt = str(datetime.timedelta(seconds=timer))
        timeDone = datetime.datetime.now()
        timeDoneStr = timeDone.strftime('%B %d, %Y %I:%M%p')

        setiniMoist = 100
        '''
        Get hours
        '''
        m, s = divmod(timer, 60)
        h, m = divmod(m, 60)
        mtoh = m * 0.0166666667
        hours = h + mtoh

        '''
        Get Date Started
        '''
        iniDateStr = iniDate.strftime('%B %d, %Y %I:%M%p')

        dryingRate = (initialw - lastw)/hours

        save = FinalData(
            batch_result=checkid,
            drying_rate=dryingRate,
            date_ended=timeDone,
            final_moisture=finalMoist,
            last_weight=lastw
            )

        session.add(save)
        session.commit()

        parent = BoxLayout(orientation = 'vertical')
        box = BoxLayout(orientation = 'horizontal')
        prestats = BoxLayout(orientation = 'vertical')
        prestata = BoxLayout(orientation = 'vertical')
        stats = BoxLayout(orientation = 'vertical')
        stata = BoxLayout(orientation = 'vertical')

        drateBox = BoxLayout(orientation = 'horizontal', size_hint = (1,0.3))
        buttonBox = BoxLayout(orientation = 'horizontal', size_hint = (1,0.3))

        prestats.add_widget(
            Label(
                text = 'Date Started:',
                color=[0, 0, 0, 1],
                )
            )
        prestats.add_widget(
            Label(
                text = 'Weight then:',
                color=[0, 0, 0, 1],
                )
            )
        prestats.add_widget(
            Label(
                text = 'Moisture Content then:',
                color=[0, 0, 0, 1],
                )
            )
        prestats.add_widget(
            Label(
                text = 'Time Elapsed:',
                color=[0, 0, 0, 1],
                )
            )


        self.DS = Label(text = str(iniDateStr), color=[0, 0, 0, 1])
        prestata.add_widget(self.DS)
        self.IW = Label(text = str(initialw) + ' kg', color=[0, 0, 0, 1])
        prestata.add_widget(self.IW)
        self.IM = Label(text = str('%.2f'%setiniMoist) + ' %', color=[0, 0, 0, 1])
        prestata.add_widget(self.IM)
        self.TE = Label(text = set_elapsedt, color=[0, 0, 0, 1])
        prestata.add_widget(self.TE)

        stats.add_widget(
            Label(
                text = 'Date Finished:',
                color=[0, 0, 0, 1],
                )
            )
        stats.add_widget(
            Label(
                text = 'Weight now:',
                color=[0, 0, 0, 1],
                )
            )
        stats.add_widget(
            Label(
                text = 'Moisture Content now:',
                color=[0, 0, 0, 1],
                )
            )
        stats.add_widget(
            Label(
                text = 'Bean Quality:',
                color=[0, 0, 0, 1],
                )
            )


        self.TD = Label(text = str(timeDoneStr), color=[0, 0, 0, 1])
        stata.add_widget(self.TD)
        self.LW = Label(text = str(lastw) + ' kg', color=[0, 0, 0, 1])
        stata.add_widget(self.LW)
        self.MC = Label(text = str('%.2f'%finalMoist) + ' %', color=[0, 0, 0, 1])
        stata.add_widget(self.MC)
        self.BQ = Image(source='img/green2.png')
        stata.add_widget(self.BQ)

        self.DR = Label(text = 'Drying Rate: '+ str('%.2f'%dryingRate) + ' kg/h', color=[0, 0, 0, 1])
        drateBox.add_widget(self.DR)

        homeButton = Button(text='Return to Home Screen')

        box.add_widget(prestats)
        box.add_widget(prestata)
        box.add_widget(stats)
        box.add_widget(stata)
        buttonBox.add_widget(homeButton)
        parent.add_widget(box)
        parent.add_widget(drateBox)
        parent.add_widget(buttonBox)
        self.add_widget(parent)

        homeButton.bind(on_press=lambda x:self.homeScreen())

    def homeScreen(self):
        global timer, checkid, lastw, iniDate, finalMoist, initialw, divider
        timer = 0
        checkid = 0
        lastw = 0
        initialw = 0
        divider = 0
        finalMoist = 100
        iniDate = None

        self.clear_widgets()
        self.manager.current = 'home'

class PresetSpinner(Spinner):
    vals = ListProperty([])
    
    def get_vals(self):
        '''
        populates the search preset spinner with values
        from the db
        '''
        print '\n\n*-- function triggered --*\n\n'

        set1 = session.query(UserPreferences)

        self.canvas.ask_update()

        qr_len = set1.count()
        val_len = len(self.vals)
        
        if qr_len > val_len:
            '''
            triggers when something has been added to the DB
            '''
            for items in set1:
                if str(items.user_batch_name) not in self.vals:
                    self.vals.append(str(items.user_batch_name))
                    print '\n\none\n\n'
        elif qr_len < val_len:
            '''
            empties the whole ListProperty
            '''
            self.vals[:] = []
            print '\n\ntwo point zero\n\n'

            '''
            then re-appends the existing values from DB
            '''
            for items in set1:
                self.vals.append(str(items.user_batch_name))
                print '\n\ntwo\n\n'
                print str(items.user_batch_name)
            self.text = 'Press to search for saved presets...'
            # val_len = set1.count()

            if not qr_len:
                print '\n\n*-- no values --*\n\n'
                self.text = 'Press to search for saved presets...'
        else:
            '''
            triggers when qr_len is equal to val_len
            must do nothing
            '''
            print '\n\nthree\n\n'

        print '\n\n*-- values --*'
        print qr_len
        print val_len

class ApplyButton(Button):
    
    def get_vals():
        pass

class LabelA(Label):
    pass

class LabelB(Label):
    pass

class LabelC(Label):
    pass

class LabelD(Label):
    pass

class LabelE(Label):
    pass

class ShapePad(Label):
    pass

class DrierApp(App):
    USER_BATCH_NAME = StringProperty('DUMMY1')
    USER_BATCH_TYPE = StringProperty('DUMMY2')
    USER_BATCH_MOISTURE = 0.0

    USER_SELECTION = StringProperty('')
    # ID of the selected preset
    INIT_WEIGHT = 0.0

    POSITIVE_COLOR = [228/255.0, 129/255.0, 68/255.0, 1]
    NEGATIVE_COLOR = [69/255.0, 44/255.0, 33/255.0, 1]
    NEUTRAL = 'img/white.png'

    icon = 'img/small_logo.png'
    title = 'Automated Cacao Dryer'


    def build(self):
        return Display()

if __name__ == '__main__':
    if not os.path.exists('db.sqlite3'):
        Base.metadata.create_all(engine)
    try:
        DrierApp().run()
    except KeyboardInterrupt:
        print 'Stopped'
        GPIO.cleanup()